import aiohttp
from sqlalchemy import select
from sqlalchemy.sql import text
from aiohttp_jinja2 import template

from .. import db


@template('index.html')
def index(request):
    return { 'site_name': request.app['config'].get('site_name') }


async def post(request):
    async with request.app['db'].acquire() as conn:
        query = select([db.post])
        result = await conn.fetch(query=query)

    return aiohttp.web.Response(body=str(result))
